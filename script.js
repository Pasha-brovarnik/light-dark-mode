const DARK_THEME = 'dark';
const LIGHT_THEME = 'light';

const toggleSwitch = setElementSelector('input[type="checkbox"]');
const nav = setElementSelector('.nav-bar');
const toggleIcon = setElementSelector('#toggle-icon');
const textBox = setElementSelector('#text-box');
const images = document.querySelectorAll('img');

// Function to select element on DOM
function setElementSelector(selector) {
	return document.querySelector(selector);
}

// Dark or Light images
function imageMode(color) {
	const srcArr = [
		`img/undraw_proud_coder_${color}.svg`,
		`img/undraw_feeling_proud_${color}.svg`,
		`img/undraw_conceptual_idea_${color}.svg`,
	];

	images.forEach((image, i) => (image.src = srcArr[i]));
}

// Set properties for light or dark theme
function toggleDarkLightMode(theme) {
	const isDark = theme === 'dark';

	nav.style.backgroundColor = isDark
		? 'rgb(0 0 0 / 50%)'
		: 'rgb(255 255 255 / 80%)';
	textBox.style.backgroundColor = isDark
		? 'rgb(255 255 255 / 80%)'
		: 'rgb(0 0 0 / 50%)';
	toggleIcon.children[0].textContent = isDark ? 'Dark mode' : 'Light mode';

	isDark
		? toggleIcon.children[1].classList.replace('fa-sun', 'fa-moon')
		: toggleIcon.children[1].classList.replace('fa-moon', 'fa-sun');

	isDark ? imageMode(DARK_THEME) : imageMode(LIGHT_THEME);
}

// Set theme properties and save to localStorage
function setTheme(mode) {
	document.documentElement.setAttribute('data-theme', mode);
	localStorage.setItem('theme', mode);
	toggleDarkLightMode(mode);
}

// Switch theme dynamically
function switchTheme({ target: { checked } }) {
	if (checked) {
		setTheme(DARK_THEME);
	} else {
		setTheme(LIGHT_THEME);
	}
}

// Event listener
toggleSwitch.addEventListener('change', switchTheme);

// Check localStorage for theme
const currentTheme = localStorage.getItem('theme');
if (currentTheme) {
	document.documentElement.setAttribute('data-theme', currentTheme);

	if (currentTheme === DARK_THEME) {
		toggleSwitch.checked = true;
		toggleDarkLightMode(DARK_THEME);
	}
}
